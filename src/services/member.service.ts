import { Workplace } from './../models/Workplace';
import { Ustatus } from './../models/Ustatues';
import { Funeralhome } from './../models/Funeralhome';
import { Member } from './../models/Member';
import { Injectable } from '@angular/core';
import { HttpModule, Http, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MemberService {
  baseUrl = 'http://localhost/laravelapi/public/api/';

  constructor(private http: Http) {}

  searchMembers(keyword): Observable<Member[]> {
    let params: URLSearchParams = new URLSearchParams();
    console.log(keyword);
    params.set('keyword', keyword);
    const url = 'http://localhost/laravelapi/public/api/search';
    return this.http
      .get(url, {
        search: params
      })
      .pipe(map(response => <Member[]>response.json()));
  }

  getMember(master): Observable<Member> {
    return this.http
      .get('http://localhost/laravelapi/public/api/' + 'members/' + master)
      .pipe(map(response => <Member>response.json()));
  }

  getCompanys(): Observable<Workplace[]> {
    return this.http
      .get('http://localhost/laravelapi/public/api/workplace')
      .pipe(map(response => <Workplace[]>response.json()));
  }

  getFhomes(): Observable<Funeralhome[]> {
    return this.http
      .get('http://localhost/laravelapi/public/api/fhomes')
      .pipe(map(response => <Funeralhome[]>response.json()));
  }

  getStatus(): Observable<Ustatus[]> {
    return this.http
      .get('http://localhost/laravelapi/public/api/ustatu')
      .pipe(map(response => <Ustatues[]>response.json()));
  }
}
