export interface Member {
  dept: string;
  zmaster: string;
  master: string;
  first: string;
  last: string;
  address: string;
  city: string;
  prov: string;
  postal: string;
  hrly: number;
  company: string;
  union_status: string;
  status_date: Date;
  hire_date: Date;
  initiation: Date;
  company_status: string;
  email: string;
  phone: string;
  cell: string;
  call_drop: boolean;
  mail: boolean;
  member_death_date: Date;
  member_funeral_date: Date;
  member_funeral_home: string;
  member_sent: string;
  spouse_name: string;
  spouse_death_date: Date;
  spouse_funeral_home: string;
  spouse_sent: string;
}
