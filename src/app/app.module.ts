import { MemberEditComponent } from './member/member-edit/member-edit.component';
import { MemberViewComponent } from './member/member-view/member-view.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OktaAuthModule } from '@okta/okta-angular';
import { HttpModule } from '@angular/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxPaginationModule } from 'ngx-pagination';

import {
  MatToolbarModule,
  MatMenuModule,
  MatIconModule,
  MatCardModule,
  MatButtonModule,
  MatTableModule,
  MatDividerModule,
  MatProgressSpinnerModule,
  MatFormField,
  MatInputModule,
  MatCheckboxModule,
  MatTabsModule,
  MatDatepickerModule
} from '@angular/material';

import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { DetailsComponent } from './details/details.component';
import { TestComponent } from './test/test.component';
import { HeaderComponent } from './header/header.component';
import { MemberService } from '../services/member.service';
import { AlertifyService } from '../services/alertify.service';
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    DetailsComponent,
    TestComponent,
    HeaderComponent,
    MemberViewComponent,
    MemberEditComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    MatCheckboxModule,
    MatButtonModule,
    MatDatepickerModule,
    MatTableModule,
    MatTabsModule,
    MatDividerModule,
    NgxPaginationModule,
    MatProgressSpinnerModule,
    AppRoutingModule,
    HttpModule,
    TextMaskModule,
    OktaAuthModule.initAuth({
      issuer: 'https://dev-555668.okta.com/oauth2/ausaz9g21xkkzzBbX356',
      redirectUri: 'http://localhost:4200/implicit/callback',
      clientId: '0oaazakkviCg1foEm356'
    })
  ],
  providers: [MemberService, AlertifyService],
  bootstrap: [AppComponent]
})
export class AppModule {}
