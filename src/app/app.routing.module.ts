import { MemberEditComponent } from './member/member-edit/member-edit.component';
import { MemberViewComponent } from './member/member-view/member-view.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { DetailsComponent } from './details/details.component';
import { OktaCallbackComponent, OktaAuthGuard } from '@okta/okta-angular';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'search', component: SearchComponent }, //canActivate: [OktaAuthGuard] },
  { path: 'implicit/callback', component: OktaCallbackComponent },
  { path: 'member-view/:master', component: MemberViewComponent },
  { path: 'member-edit/:master', component: MemberEditComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
