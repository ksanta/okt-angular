import { async } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OktaAuthService } from '@okta/okta-angular';

interface Claim {
  claim: any;
  value: any;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isAuthenticated: boolean;
  idToken;
  claims: Array<Claim>;
  userClaims: any;
  name: string;
  slevel = 0;

  constructor(private router: Router, public oktaAuth: OktaAuthService) {
    this.oktaAuth.$authenticationState.subscribe(
      (isAuthenticated: boolean) => (this.isAuthenticated = isAuthenticated)
    );
  }

  ngOnInit() {
    this.oktaAuth.isAuthenticated().then(auth => {
      this.isAuthenticated = auth;
      this.getWhoLogin();
    });
  }

  async getWhoLogin() {
    this.userClaims = await this.oktaAuth.getUser();
    this.claims = Object.entries(this.userClaims).map(entry => ({
      claim: entry[0],
      value: entry[1]
    }));
    console.log(this.claims);
    this.name = this.claims[3].value;
    this.slevel = this.claims[4].value;
  }

  login() {
    this.oktaAuth.loginRedirect();
  }

  logout() {
    this.name = '';
    this.slevel = 0;
    this.isAuthenticated = false;
    this.oktaAuth.logout('/');
  }
}
