import { AlertifyService } from './../../services/alertify.service';
import { Member } from './../../models/Member';
import { MemberService } from './../../services/member.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  keyword = '';
  members = [];
  isLoading = false;
  p = 1;
  recount = 1;

  constructor(
    private memberService: MemberService,
    private alertify: AlertifyService
  ) {}

  ngOnInit() {}

  getKeyword(keyword) {
    this.isLoading = true;
    this.memberService
      .searchMembers(this.keyword)
      .subscribe((members: Member[]) => {
        this.members = members;
        this.recount = members.length;
        if (this.recount === 0) {
          this.alertify.warning('No Records Found!');
        }
        this.isLoading = false;
        this.keyword = '';
      });
  }
}
