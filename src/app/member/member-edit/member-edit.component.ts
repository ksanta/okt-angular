import { MemberService } from './../../../services/member.service';
import { Component, OnInit } from '@angular/core';
import { Member } from '../../../models/Member';
import { ActivatedRoute } from '@angular/router';
import { AlertifyService } from '../../../services/alertify.service';
import { Workplace } from '../../../models/Workplace';
import { Ustatus } from '../../../models/Ustatus';
import { Funeralhome } from '../../../models/Funeralhome';

@Component({
  selector: 'app-member-edit',
  templateUrl: './member-edit.component.html',
  styleUrls: ['./member-edit.component.css']
})
export class MemberEditComponent implements OnInit {
  member: Member;
  mask: any[] = [
    '(',
    /[1-9]/,
    /\d/,
    /\d/,
    ')',
    ' ',
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/
  ];
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$';
  workplaces: Workplace[];
  ustatus: UStatus[];
  funeralhomes: Funeralhome[];
  model: any = {};

  constructor(
    private memberService: MemberService,
    private route: ActivatedRoute,
    private alertify: AlertifyService
  ) {}

  ngOnInit() {
    this.loadMember();
    this.loadCompanys();
    this.loadUstatus();
    this.loadFuneralHomes();
  }

  loadMember() {
    this.memberService
      .getMember(this.route.snapshot.params['master'])
      .subscribe(
        (member: Member) => {
          this.member = member;
        },
        error => {
          this.alertify.error('Cannot Retrieve Member Information');
        }
      );
  }

  loadCompanys() {
    this.memberService.getCompanys().subscribe(
      (workplaces: Workplace[]) => {
        this.workplaces = workplaces;
      },
      error => {
        this.alertify.error('could not pull the list of companies.');
      }
    );
  }

  loadUstatus() {
    this.memberService.getStatus().subscribe(
      (ustatus: UStatus[]) => {
        this.ustatus = ustatus;
        console.log(this.ustatus);
      },
      error => {
        this.alertify.error('could not pull the status list.');
      }
    );
  }

  loadFuneralHomes() {
    this.memberService.getFhomes().subscribe(
      (funeralhomes: Funeralhome[]) => {
        this.funeralhomes = funeralhomes;
      },
      error => {
        this.alertify.error('could not pull the Funeral Homes.');
      }
    );
  }

  updateMember() {
    this.memberService
      .updateMember(this.route.snapshot.params['master'], this.member)
      .subscribe(
        next => {
          this.alertify.success('profile updated successfully.');
        },
        error => {
          this.alertify.error('profile could not be saved.');
        }
      );
  }
}
