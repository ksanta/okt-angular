import { MemberService } from './../../../services/member.service';
import { Component, OnInit } from '@angular/core';
import { Member } from '../../../models/Member';
import { ActivatedRoute } from '@angular/router';
import { AlertifyService } from '../../../services/alertify.service';

@Component({
  selector: 'app-member-view',
  templateUrl: './member-view.component.html',
  styleUrls: ['./member-view.component.css']
})
export class MemberViewComponent implements OnInit {
  member: Member;

  constructor(
    private memberService: MemberService,
    private route: ActivatedRoute,
    private alertify: AlertifyService
  ) {}

  ngOnInit() {
    this.loadMember();
  }

  loadMember() {
    this.memberService
      .getMember(this.route.snapshot.params['master'])
      .subscribe(
        (member: Member) => {
          this.alertify.success('member loaded!');
          this.member = member;
        },
        error => {
          this.alertify.error('there has been an error');
        }
      );
  }
}
